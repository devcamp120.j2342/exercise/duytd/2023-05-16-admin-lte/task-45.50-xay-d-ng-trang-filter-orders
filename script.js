$(document).ready(function(){
    //REGION 1
    const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
    
    const gCOLUMN_ORDERCODE = 0;
    const gCOLUMN_KICH_CO = 1;
    const gCOLUMN_LOAI_PIZZA = 2;
    const gCOLUMN_LOAI_NUOC_UONG = 3;
    const gCOLUMN_THANH_TIEN = 4 ;
    const gCOLUMN_HO_TEN = 5;
    const gCOLUMN_SO_DIEN_THOAI = 6;
    const gCOLUMN_TRANG_THAI = 7
    const gCOLUMN_ACTION = 8;

    const gCOL_NAME = [
        "orderCode",
        "kichCo",
        "loaiPizza",
        "idLoaiNuocUong",
        "thanhTien",
        "hoTen",
        "soDienThoai",
        "trangThai",
        "antion"
    ]
    var gOrderTable = $("#table-order").DataTable( {
        // Khai báo các cột của datatable
        "columns" : [
            { "data" : gCOL_NAME[gCOLUMN_ORDERCODE]},
            { "data" : gCOL_NAME[gCOLUMN_KICH_CO]},
            { "data" : gCOL_NAME[gCOLUMN_LOAI_PIZZA]},
            { "data" : gCOL_NAME[gCOLUMN_LOAI_NUOC_UONG]},
            { "data" : gCOL_NAME[gCOLUMN_THANH_TIEN]},
            { "data" : gCOL_NAME[gCOLUMN_HO_TEN]},
            { "data" : gCOL_NAME[gCOLUMN_SO_DIEN_THOAI]},
            { "data" : gCOL_NAME[gCOLUMN_TRANG_THAI]},
            { "data" : gCOL_NAME[gCOLUMN_ACTION]}
        ],
        // Ghi đè nội dung của cột action, chuyển thành button chi tiết
        "columnDefs": [ 
        {
            "targets": gCOLUMN_ACTION,
            "defaultContent": "<button class='info-order btn btn-info'>Chi tiết</button>"
        }]
    });
    
    var gList_Order = [];
    var gOrder_Code = "";
    var gOrder_Id = "";
    //REGION 2
    onPageLoading();
    //gán sự kiện nút btn detail click
    $("#table-order").on('click', '.info-order', function(){
        onBtnDetailClick(this);
    })
    //gán sự kiện nút btn filter click
    $("#btn-filter").on('click', function(){
        onBtnFilterClick();
    })
    //gán sự kiện nút btn confirm được click
    $("#btn-confirm").on('click', function(){
        onBtnConfirmClick();
    })
    //gán sự kiện nút cancel được click
    $("#btn-cancel").on('click', function(){
        onBtnCancelClick();
    })
    //REGION 3
    function onPageLoading(){
        callApiGetDataOrder();
    }
    //hàm thực hiện chức năng nút filter được click
    function onBtnFilterClick(){
        // b1 chuẩn bị dữ liệu
        var vOrderObj = {
            trangThai: "",
            loaiPizza: ""
        }
        //thu thập dữ liệu
        vOrderObj.trangThai = $("#select-order").val();
        vOrderObj.loaiPizza = $("#select-loai-pizza").val();
        //b2 tiến hành lọc dữ liệu
        filterOrderTable(vOrderObj);
    }
    //hàm thự hiện sự kiện nút btn chi tiết được click
    function onBtnDetailClick(paramDetail){
        //lấy dữ liệu trên dataTable khi nút detail được ấn
        getDataBtnDetailTable(paramDetail);
        //gọi api lấy order thông qua orderCode
        callApiGetOrderByOrderCode()

        
    }
    //hàm thự hiện chức năng nút confirm được click
    function onBtnConfirmClick(){
        console.log(gOrder_Id);
        //b1 chuẩn bị dữ liệu, thu thập dữ liệu
        var vStatusObj = {
            trangThai: "confirmed",
        }
        //b2 xử lý dữ liệu đầu vào, đầu ra
        //b3 gọi api
        $.ajax({
            url: gBASE_URL + "/" + gOrder_Id,
            type: "PUT",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(vStatusObj),
            success: function(response){
                console.log(response);
                $("#modal-order-detail").modal('hide'); // Ẩn modal
            },
            error: function(err){
                console.log(err.status);
            }
        });
    }
    //hàm thự hiện chức năng nút cancel được click
    function onBtnCancelClick(){
        console.log(gOrder_Id);
        //b1 chuẩn bị dữ liệu, thu thập dữ liệu
        var vStatusObj = {
            trangThai: "cancel",
        }
        //b2 xử lý dữ liệu đầu vào, đầu ra
        //b3 gọi api
        $.ajax({
            url: gBASE_URL + "/" + gOrder_Id,
            type: "PUT",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(vStatusObj),
            success: function(response){
                console.log(response);
                //b4 xử lý hiển thị
                $("#modal-order-detail").modal('hide'); // Ẩn modal
            },
            error: function(err){
                console.log(err.status);
            }
        });
    }
    //REGION 4
    //hàm gọi api để lấy tất cả dữ liệu order
    function callApiGetDataOrder(){
        $.ajax({
            url: gBASE_URL,
            type: "GET",
            success: function(response){
                gList_Order = response;
                loadDataToTable(response);
                console.log(gList_Order);
            },
            error: function(err){
                console.log(err.status);
            }
        });
    }
    //hàm load dữ liệu vào dataTable
    function loadDataToTable(paramData){
        gOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        gOrderTable.rows.add(paramData) // cập nhật dữ liệu cho bảng
        gOrderTable.draw() //vẽ lại bảng;

    }
    //hàm lọc dữ liệu order
    function filterOrderTable(paramOrderObj) {
        // Lấy giá trị firstname và lastname từ đối tượng userObj
        var vTrangThai = paramOrderObj.trangThai;
        var vLoaiPizza = paramOrderObj.loaiPizza;
  
        // Lọc và tìm kiếm dữ liệu trong bảng DataTables
        gOrderTable.search(vTrangThai + " " + vLoaiPizza).draw();
    }
    //hàm thu thập dữ liệu ở dataTable khi nút btn detail được ấn
    function getDataBtnDetailTable(paramObj){
        //Xác định thẻ tr là cha của nút được chọn
        var vRowSelected = $(paramObj).parents('tr');
        //Lấy datatable row
        var vDatatableRow = gOrderTable.row(vRowSelected); 
        //Lấy data của dòng 
        var vOrderData = vDatatableRow.data();
        var vOrderCode = vOrderData.orderCode;
        gOrder_Code = vOrderCode;
        var vKichCoCombo = vOrderData.kichCo;  
        var vLoaiPizza = vOrderData.loaiPizza;    
        var vNuocUong = vOrderData.idLoaiNuocUong;
        var vThanhTien = vOrderData.thanhTien;
        var vHoVaTen = vOrderData.hoTen;    
        var vSoDienThoai = vOrderData.soDienThoai;
        var vTrangThai = vOrderData.trangThai;

        console.log(gOrder_Code);
        console.log(vKichCoCombo);
        console.log(vLoaiPizza);
        console.log(vNuocUong);
        console.log(vThanhTien);
        console.log(vHoVaTen);
        console.log(vSoDienThoai);
        console.log(vTrangThai);
        $("#modal-order-detail").modal('show');
    }
    //hàm gọi api để lấy order thông qua orderCode
    function callApiGetOrderByOrderCode(){
        $.ajax({
            url : gBASE_URL + "/" + gOrder_Code,
            type: "GET",
            success: function(response){
                showDataToMoDalOrderDetail(response);
                gOrder_Id = response.id;
            },
            error: function(err){
                console.log(err.status)
            }
        })
    }
    //hàm hiển thị dữ liệu lên các trường trên modal order detail
    function showDataToMoDalOrderDetail(paramRes){
        $("#inp-order-code").val(paramRes.orderCode);
        $("#select-pizza-size").val(paramRes.kichCo);
        $("#inp-duong-kinh").val(paramRes.duongKinh);
        $("#inp-suon").val(paramRes.suon);
        $("#inp-Salad").val(paramRes.salad);
        $("#inp-pizza-type").val(paramRes.loaiPizza);
        $("#inp-id-voucher").val(paramRes.idVourcher);
        $("#inp-thanh-tien").val(paramRes.thanhTien);
        $("#inp-giam-gia").val(paramRes.giamGia);
        $("#select-drink").val(paramRes.idLoaiNuocUong);
        $("#inp-so-luong-nuoc").val(paramRes.soLuongNuoc);
        $("#inp-ho-ten").val(paramRes.hoTen);
        $("#inp-email").val(paramRes.email);
        $("#inp-phone").val(paramRes.soDienThoai);
        $("#inp-dia-chi").val(paramRes.diaChi);
        $("#inp-message").val(paramRes.loiNhan);
        $("#inp-ngay-order").val(paramRes.ngayTao);
        $("#inp-ngay-chinh-sua").val(paramRes.ngayCapNhat)
    }
    
    });
    